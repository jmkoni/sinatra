require 'rubygems'
require 'sinatra'

get '/' do
	@title = "Enter your text here"
	erb :home
end

get '/frank' do
	@title = "My Way"
	erb :frank
end

post '/' do
# 	params.inspect
	@title = "Here's your reversed text"
	@revtext = params[:phrase].reverse
	erb :reverse
end

get '/:phrase' do
# 	params.inspect
	@title = "Here's your reversed text"
	@revtext = params[:phrase].reverse
	erb :reverse
end

# This is all from prior to splitting it up into a views folder

# use_in_file_templates!
# When I tried to use that, got this error:
# reverse/main.rb:8:in `<main>': undefined method `use_in_file_templates!' for main:Object (NoMethodError)

# __END__
# 
# @@ home
# 
# <h1>Reverse</h1>
# 
# <p>Welcome to the very first page of my Sinatra app!</p>

