require 'rubygems'
require 'sinatra'
require 'data_mapper'
# require 'dm-core'
# require 'dm-migrations'

DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/development.db")

class Task
  include DataMapper::Resource

  property :id,           Serial
  property :name,         String
  property :completed_at, DateTime
end

# new task
get '/task/new' do
	erb :new
end

# create a new task
post '/task/create' do
	task = Task.new(:name => params[:name])
	if task.save
		status 201
		redirect '/task/' + task.id.to_s
	else
		status 412
		redirect '/tasks'
	end
end

# view task
get '/task/:id' do
	@task = Task.get(params[:id])
	erb :task
end

get '/tasks' do
end

DataMapper.auto_upgrade!